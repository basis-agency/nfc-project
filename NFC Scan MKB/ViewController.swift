//
//  ViewController.swift
//  NFC Scan MKB
//
//  Created by Sergey Melnik on 18.04.2023.
//

import UIKit
import CoreNFC
import SwiftyJSON

class ViewController: UIViewController, NFCTagReaderSessionDelegate  {
    
    @IBOutlet weak var NFCText: UITextView!
    
    var nfcTagReaderSession: NFCTagReaderSession?
    var nfcSession: NFCTagReaderSession?
    var word = "none"
    
    @IBAction func ScanBtn(_ sender: Any) {
        
        guard NFCTagReaderSession.readingAvailable else {
                    let alertController = UIAlertController(
                        title: "Скнирование не поддерживается",
                        message: "Устройство не поддерживает сканирование Вашей карты.",
                        preferredStyle: .alert
                    )

                    alertController.addAction(UIAlertAction(title: "ОК", style: .default, handler: nil))
                    self.present(alertController, animated: true, completion: nil)
                    return
                }
        
        nfcTagReaderSession = NFCTagReaderSession(pollingOption: [.iso14443], delegate: self)
        nfcTagReaderSession?.begin()
        print("isReady: \(String(describing: nfcTagReaderSession?.isReady))")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NFCText.textColor = .green
        NFCText.text = ""
        NFCText.isEditable = false
    }
     
    func connect( to tag: NFCTag, completionHandler: @escaping ((Error)?) -> Void) {}
    
    func tagReaderSessionDidBecomeActive(_ session: NFCTagReaderSession) {
        print("Сессия стартовала")
    }
    
    func tagReaderSession(_ session: NFCTagReaderSession, didInvalidateWithError error: Error) {
        print("Сессия отвалилась из-за: \(error.localizedDescription)")
    }
    
    func tagReaderSession(_ session: NFCTagReaderSession, didDetect tags: [NFCTag]) {
        
            if case let NFCTag.iso7816(tag) = tags.first! {
                
                //3
                session.connect(to: tags.first!) { (error: Error?) in
                    
                    //4
                    let myAPDU = NFCISO7816APDU(instructionClass:0, instructionCode:0xB0, p1Parameter:0, p2Parameter:0, data: Data(), expectedResponseLength:16)
                    tag.sendCommand(apdu: myAPDU) { (response: Data, sw1: UInt8, sw2: UInt8, error: Error?)
                        in
                        print("Ответ с карты: \(response.base64EncodedString())")
                        
                        guard error != nil && !(sw1 == 0x90 && sw2 == 0) else {
                            session.invalidate(errorMessage: "Application failure")
                            return
                        }
                    }
                }
            }
        }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
