Документация по проекту MKB NFC

1. Для работы с NFC требуется подключить библиотеку CoreNFC
2. Модуль, в котором будет обрабатываться сканирование и обработка данных с NFC чипа должен наследоваться от протокола NFCTagReaderSessionDelegate

3. Платежный NFC сканируется по стандарту iso 7816
Ссылка на офф документацию Apple по этому стандарту: https://developer.apple.com/documentation/corenfc/nfciso7816tag?language=Objc

Для того, чтобы данные карты могли сканироваться необходимо прописать в info.plist AID'ы всех карт в параметре ISO7816 application identifiers for NFC Tag Reader Session.

AID - Application Identification (идентификатор приложения конкретной карты(Visa, Mastercard, Mir и т.д.))

Дальше перечисляем AID'ы:
Ниже пример кода из файла info.plist для удобства копирования:
<key>com.apple.developer.nfc.readersession.iso7816.select-identifiers</key>
	<array>
		<string>A0000000031010</string>
		<string>A0000000032010</string>
		<string>A0000000032020</string>
		<string>A0000000038010</string>
		<string>A0000000041010</string>
		<string>A0000000049999</string>
		<string>A0000000043060</string>
		<string>A0000000046000</string>
		<string>A0000000048002</string>
		<string>A0000000050001</string>
		<string>A00000002501</string>
		<string>A00000079001</string>
		<string>A0000000980840</string>
		<string>A0000000042203</string>
		<string>A0000001524010</string>
		<string>A000000817002001</string>
		<string>A0000000421010</string>
		<string>A0000000422010</string>
		<string>A0000000651010</string>
		<string>A0000001211010</string>
		<string>A0000001214711</string>
		<string>A0000001214712</string>
		<string>A0000001410001</string>
		<string>A0000001523010</string>
		<string>A0000001544442</string>
		<string>A0000002281010</string>
		<string>A0000002771010</string>
		<string>A0000003241010</string>
		<string>A000000333010101</string>
		<string>A000000333010102</string>
		<string>A000000333010103</string>
		<string>A000000333010106</string>
		<string>A0000003591010028001</string>
		<string>A00000035910100380</string>
		<string>A0000003710001</string>
		<string>A0000004391010</string>
		<string>A0000005241010</string>
		<string>A0000006300101</string>
		<string>A0000006582010</string>
		<string>A0000006581010</string>
		<string>A0000004360100</string>
		<string>A00000038410</string>
		<string>A00000038420</string>
		<string>A000000337301000</string>
		<string>A000000337101000</string>
		<string>A000000337102000</string>
		<string>A000000337102001</string>
		<string>A000000337601001</string>
		<string>A000000732100123</string>
		<string>A0000005291010</string>
	</array>

4. Необходимо имплементировать следующие методы для работы сканирования NFC:
connect
tagReaderSessionDidBecomeActive
func connect( to tag: NFCTag, completionHandler: @escaping ((Error)?) -> Void) {}
func tagReaderSessionDidBecomeActive(_ session: NFCTagReaderSession) - метод для уведомления, что сессия стартовала
func tagReaderSession(_ session: NFCTagReaderSession, didInvalidateWithError error: Error) - метод для уведомления об падении сессии
func tagReaderSession(_ session: NFCTagReaderSession, didDetect tags: [NFCTag]) - метод, в котором идет обработка сканирования NFC с вызовом модального окна



Справочная информация:
https://swifting.io/2019/07/11/NFC-iOS-13.html
https://developer.apple.com/documentation/corenfc/nfciso7816tag?language=Objc
https://stackoverflow.com/questions/76495373/is-it-possible-to-read-the-card-number-and-expiration-date-from-sberbank-and-tin
https://en.wikipedia.org/wiki/Smart_card_application_protocol_data_unit
https://www.youtube.com/watch?v=DIG6YDP6w5U
https://github.com/cuamckuu/nfc-frog/blob/master/device_nfc.cpp
https://habr.com/ru/companies/koshelek/articles/515602/
https://en.wikipedia.org/wiki/EMV#Application_selection